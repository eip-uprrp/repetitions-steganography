#ifndef STEGAPANEL_H
#define STEGAPANEL_H

#include <QMainWindow>
#include <QFileDialog>
#include <QDir>
#include <QMessageBox>
#include <QSlider>
#include <QShortcut>
#include <QMessageBox>
#include <string>

using namespace std ;

namespace Ui {
class StegaPanel;
}

class StegaPanel : public QMainWindow
{
    Q_OBJECT

public:
    /// \fn StegaPanel::StegaPanel(QWidget *parent)
    /// \~English
    /// \brief GUI Constructor
    /// \~Spanish
    /// \brief Constructor del GUI
    explicit StegaPanel(QWidget *parent = 0);

    /// \fn void StegaPanel::hideMessage()
    /// \~English
    /// \brief Function that calls the message embedding function.
    /// \~Spanish
    /// \brief Funcion que llama la funcion de embedir el mensaje.
    ///
    void hideMessage();

    /// \fn void StegaPanel::revealMessage()
    /// \~English
    /// \brief Function that calls the message extraction function.
    /// \~Spanish
    /// \brief Funcion que llama la funcion de extraer el mensaje.
    ///
    void revealMessage();

    /// \fn StegaPanel::~StegaPanel()
    /// \~English
    /// \brief GUI Destructor
    /// \~Spanish
    /// \brief Destructor del GUI
    ~StegaPanel();

private slots:
    /// \fn void StegaPanel::on_loadImage_clicked()
    /// \~English
    /// \brief Event function to load an image from the
    ///                    file system.
    /// \~Spanish
    /// \brief Funcion de evento que carga una imagen de el
    ///                    sistema de archivos.
    ///
    void on_loadImage_clicked();

    /// \fn void StegaPanel::on_hideMessage_clicked()
    /// \~English
    /// \brief Event function to hide message into
    ///                     the image.
    /// \~Spanish
    /// \brief Funcion de evento para esconder e
    ///         el mensaje en la imagen.
    ///
    void on_hideMessage_clicked();

    /// \fn void StegaPanel::on_getMessage_clicked()
    /// \~English
    /// \brief Event function to extract the message hidden
    ///                     in the image.
    /// \~Spanish
    /// \brief Funcion de evento para estraer el mensaje
    ///                     escondido en la imagen.
    ///
    void on_getMessage_clicked();

    /// \fn void StegaPanel::on_storeImage_clicked()
    /// \~English
    /// \brief on_storeImage_clicked - Event function to save the new image in the
    ///                             file system.
    /// \~Spanish
    /// \brief Funcion de evento para salvar la imagen nueva
    ///                             en el sistema de archivos.
    ///
    void on_storeImage_clicked();

private:
    Ui::StegaPanel *ui;
    QImage old_image;
    QImage new_image;
    QString orig_msg;

};

#endif // MAINWINDOW_H
